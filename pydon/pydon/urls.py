from django.contrib import admin
from django.urls import include, path
from api import urls as api_urls

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('pdadmin.urls')),
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
