from django.db import models

    
class Category(models.Model):
    def upload_to(instance, filename):
        return 'cat/{filename}'.format(filename=filename)

    cat = models.CharField(max_length=60)
    cat_image = models.ImageField(upload_to="cat/", default="cat/default.png")

class Article(models.Model):
    cat = models.IntegerField()
    art = models.CharField(max_length=60)
    price = models.FloatField()

class Racun(models.Model):
    sto = models.CharField(max_length=16)
    ukupno = models.FloatField()
    operater = models.IntegerField(default=0)
    isporucen = models.BooleanField(default=False)
    naplacen = models.BooleanField(default=False)
    articles = models.ManyToManyField(Article, through='stavke')

class Stavke(models.Model):
    racun = models.ForeignKey(Racun, on_delete=models.CASCADE)
    article =models.ForeignKey(Article, on_delete=models.CASCADE)

