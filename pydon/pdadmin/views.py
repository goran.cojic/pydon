from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from .models import Category, Article
from django.shortcuts import render
from .forms import CatForm

def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render({}, request))

def pdadmin(request):
    categories = Category.objects.all().values()
    articles = Article.objects.all().values()
    template = loader.get_template('pdadmin.html')
    context = {
        'categories': categories,
        'articles': articles,
    }
    return HttpResponse(template.render(context, request))

def panel(request):
    categories = Category.objects.all().values()
    template = loader.get_template('panel.html')
    context = {
        'categories': categories,
    }
    return HttpResponse(template.render(context, request))

def operater(request):
    categories = Category.objects.all().values()
    template = loader.get_template('operater.html')
    context = {
        'categories': categories,
    }
    return HttpResponse(template.render(context, request))

def kasa(request):
    categories = Category.objects.all().values()
    template = loader.get_template('kasa.html')
    context = {
        'categories': categories,
    }
    return HttpResponse(template.render(context, request))

# Cat
# def addcat(request):
#     template = loader.get_template('addcat.html')
#     return HttpResponse(template.render({}, request))

# def addcatpost(request):
#     x = request.POST['cat']
#     item = Category(cat=x)
#     item.save()
#     return HttpResponseRedirect(reverse('pdadmin'))

def addcat(request):
    if request.method == 'POST':
        form = CatForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('pdadmin'))
    else:
        form = CatForm()

    return render(request, 'addcat.html', {'form': form})


def updatecat(request, id):
    item = Category.objects.get(id=id)
    template = loader.get_template('updatecat.html')
    context = {
        'category': item,
    }
    return HttpResponse(template.render(context, request))

def updatecatpost(request, id):
    cat = request.POST['cat']
    item = Category.objects.get(id=id)
    item.cat = cat
    item.save()
    return HttpResponseRedirect(reverse('pdadmin'))

def deletecat(request, id):
    item = Category.objects.get(id=id)
    item.cat_image.delete()
    item.delete()
    return HttpResponseRedirect(reverse('pdadmin'))

# Art
def addart(request):
    categories = Category.objects.all().values()
    template = loader.get_template('addart.html')
    context = {
        'categories': categories,
    }
    return HttpResponse(template.render(context, request))

def addartpost(request):
    x = request.POST['cat']
    #item1 = Category(cat=x)
    #item1.save()

    art = request.POST['art']
    price = request.POST['price']
    item = Article(cat=x, art=art, price=price)
    item.save()
    return HttpResponseRedirect(reverse('pdadmin'))

def updateart(request, id):
    categories = Category.objects.all().values()
    item = Article.objects.get(id=id)
    template = loader.get_template('updateart.html')
    context = {
        'categories': categories,
        'article': item,
    }
    return HttpResponse(template.render(context, request))

def updateartpost(request, id):
    cat = request.POST['cat']
    art = request.POST['art']
    price = request.POST['price']
    item = Article.objects.get(id=id)
    item.cat = cat
    item.art = art
    item.price = price
    item.save()
    return HttpResponseRedirect(reverse('pdadmin'))

def deleteart(request, id):
    item = Article.objects.get(id=id)
    item.delete()
    return HttpResponseRedirect(reverse('pdadmin'))

