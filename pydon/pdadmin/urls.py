from django.urls import path
from . import views

urlpatterns = [
  path('', views.index, name='index'),
  path('pdadmin/', views.pdadmin, name='pdadmin'),
  path('panel/', views.panel, name='panel'),
  path('operater/', views.operater, name='operater'),
  path('kasa/', views.kasa, name='kasa'),
  path('pdadmin/addcat/', views.addcat, name='addcat'),
  path('pdadmin/addart/', views.addart, name='addart'),
  #path('pdadmin/addcat/addcatpost/', views.addcatpost, name='addcatpost'),
  path('pdadmin/addart/addartpost/', views.addartpost, name='addartpost'),
  path('pdadmin/deletecat/<int:id>', views.deletecat, name='deletecat'),
  path('pdadmin/deleteart/<int:id>', views.deleteart, name='deleteart'),
  path('pdadmin/updatecat/<int:id>', views.updatecat, name='updatecat'),
  path('pdadmin/updatecat/updatecatpost/<int:id>', views.updatecatpost, name='updatecatpost'),
  path('pdadmin/updateart/<int:id>', views.updateart, name='updateart'),
  path('pdadmin/updateart/updateartpost/<int:id>', views.updateartpost, name='updateartpost'),
]