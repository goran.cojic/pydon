from django import forms
from .models import Category

#class CatForm(forms.Form):
    #cat = forms.CharField(label='Category', max_length=60)

class CatForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('cat', 'cat_image')
        labels = {
            "cat": "Category",
            "cat_image": "CatImage"
        }
