from django.apps import AppConfig


class PdadminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pdadmin'
