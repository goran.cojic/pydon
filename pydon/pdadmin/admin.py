from django.contrib import admin

from pdadmin.models import Article, Category, Racun, Stavke

admin.site.register(Category)
admin.site.register(Article)
admin.site.register(Racun)
admin.site.register(Stavke)
