#from django.conf.urls import url
from django.urls import path #, include
from .views import (
    ArticlesApiView,
    CatArticlesApiView,
    RacunApiView,
    NeplaceniApiView,
    RnStavkeApiView,
    NaplatiApiView,
    ZaIsporuku1ApiView,
    IsporuciApiView,
)

from . import views

urlpatterns = [
    path('articles', ArticlesApiView.as_view()),
    path('catarticles/<int:cat_id>/', CatArticlesApiView.as_view()),
    path('racun', RacunApiView.as_view()),
    path('neplaceni', NeplaceniApiView.as_view()),
    path('rnstavke/<int:rn_id>/', RnStavkeApiView.as_view()),
    path('naplati', NaplatiApiView.as_view()),
    path('zaisporuku1', ZaIsporuku1ApiView.as_view()),
    path('isporuci', IsporuciApiView.as_view()),
]