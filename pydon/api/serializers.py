from rest_framework import serializers
from pdadmin.models import Article, Racun, Stavke

class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = '__all__'
        #fields = ("id", "art", 'cat', 'price')

class RacunCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Racun
        fields = '__all__'
        #fields = ['id', 'sto', 'ukupno', 'operater', 'isporucen', 'naplacen']

class RacunRSerializer(serializers.ModelSerializer):
    #articles = ArticleSerializer(many=True, read_only=True)
    class Meta:
        model = Racun
        fields = ['id', 'sto', 'ukupno', 'operater', 'isporucen', 'naplacen']
        depth = 1

class RnStavkeRSerializer(serializers.ModelSerializer):
    articles = ArticleSerializer(many=True, read_only=True)
    class Meta:
        model = Racun
        fields = ['id', 'sto', 'ukupno', 'operater', 'isporucen', 'naplacen', 'articles']
        depth = 1
