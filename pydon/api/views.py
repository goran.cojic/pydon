from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from pdadmin.models import Category, Article, Racun, Stavke
from .serializers import ArticleSerializer, RacunCSerializer, RacunRSerializer, RnStavkeRSerializer


class ArticlesApiView(APIView):
    # add permission to check if user is authenticated
    ##permission_classes = [permissions.IsAuthenticated]

    # 1. List all
    def get(self, request, *args, **kwargs):
        art = Article.objects.all()
        serializer = ArticleSerializer(art, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class CatArticlesApiView(APIView):
    # add permission to check if user is authenticated
    #permission_classes = [permissions.IsAuthenticated]

    def get_object(self, cat_id):
        '''
        Helper method to get the object with given cat_id
        '''
        try:
            return Article.objects.filter(cat=cat_id)
        except Article.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, cat_id, *args, **kwargs):
        '''
        Retrieves the Cat with given cat_id
        '''
        cat_instance = self.get_object(cat_id)
        if not cat_instance:
            return Response(
                {"res": "Object with category id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ArticleSerializer(cat_instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class RacunApiView(APIView):
    #permission_classes = [permissions.IsAuthenticated]

    # 1. List racuni
    def get(self, request, *args, **kwargs):
        racuni = Racun.objects.all()
        serializer = RacunRSerializer(racuni, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 2. Create racuni + stavke
    # def post(self, request, *args, **kwargs):
    #     data = {
    #         'sto': request.data.get('sto'), 
    #         'ukupno': request.data.get('ukupno'),
    #         'operater': 1,
    #         'articles': request.data.get('stavke')
    #     }

    #     serializer = RacunCSerializer(data=data)
    #     if serializer.is_valid():
    #         s = serializer.save()

    #         return Response(serializer.data, status=status.HTTP_201_CREATED)

    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    
    # 2. Create racuni + stavke
    def post(self, request, *args, **kwargs):
        data = {
            'sto': request.data.get('sto'), 
            'ukupno': request.data.get('ukupno')
        }

        serializer = RacunCSerializer(data=data)
        if serializer.is_valid():
            s = serializer.save()
            for x in request.data.get('stavke'):
                item = Stavke(racun_id=s.id, article_id=x)
                item.save()

            data['stavke'] = request.data.get('stavke')
            return Response(data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 2. Create racuni + stavke
    # def post(self, request, *args, **kwargs):
    #     data = {
    #         'sto': request.data.get('sto'), 
    #         'ukupno': request.data.get('ukupno'),
    #         'operater': 1
    #     }

    #     serializer = RacunSerializer(data=data)
    #     if serializer.is_valid():
    #         s = serializer.save()

    #         stavke = []
    #         for x in request.data.get('stavke'):
    #             stavke.append({
    #                 'racun_id': s.id,
    #                 'article_id': x
    #             })

    #         st = StavkeSerializer(data=stavke, many=True)
    #         if st.is_valid():
    #             st.save()

    #         data['stavke'] = stavke
    #         return Response(data, status=status.HTTP_201_CREATED)

    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class NeplaceniApiView(APIView):
    #permission_classes = [permissions.IsAuthenticated]

    # 1. List neplaceni racuni
    def get(self, request, *args, **kwargs):
        racuni = Racun.objects.filter(naplacen=False)
        serializer = RacunRSerializer(racuni, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class RnStavkeApiView(APIView):
    #permission_classes = [permissions.IsAuthenticated]

    # 1. Rcun ID - stavke
    def get(self, request, rn_id, *args, **kwargs):
        rn = Racun.objects.filter(id=rn_id)
        serializer = RnStavkeRSerializer(rn, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class NaplatiApiView(APIView):
    # add permission to check if user is authenticated
    #permission_classes = [permissions.IsAuthenticated]

    def get_object(self, rn_id):
        try:
            return Racun.objects.get(id=rn_id)
        except Racun.DoesNotExist:
            return None

    # 4. Update
    def put(self, request, *args, **kwargs):
        rn = self.get_object(request.data.get('rn_id'))
        if not rn:
            return Response(
                {"res": "Object with rn_id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'naplacen': True
        }
        serializer = RacunRSerializer(instance = rn, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ZaIsporuku1ApiView(APIView):
    # add permission to check if user is authenticated
    #permission_classes = [permissions.IsAuthenticated]

    # 4. Update
    def put(self, request, *args, **kwargs):
        rn = Racun.objects.filter(operater=0).first()
        if not rn:
            return Response(
                {"OK: ": "Nema neisporucenih"}, 
                status=status.HTTP_200_OK
            )
        data = {
            'operater': int(request.data.get('op_id'))
        }
        serializer = RacunCSerializer(instance = rn, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()

            rn_get = Racun.objects.filter(id=rn.id)
            serializer = RnStavkeRSerializer(rn_get, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class IsporuciApiView(APIView):
    # add permission to check if user is authenticated
    #permission_classes = [permissions.IsAuthenticated]

    def get_object(self, rn_id):
        try:
            return Racun.objects.get(id=rn_id)
        except Racun.DoesNotExist:
            return None

    # 4. Update
    def put(self, request, *args, **kwargs):
        rn = self.get_object(request.data.get('rn_id'))
        if not rn:
            return Response(
                {"res": "Object with rn_id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'isporucen': True
        }
        serializer = RacunRSerializer(instance = rn, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)